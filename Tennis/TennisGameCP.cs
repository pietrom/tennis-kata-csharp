namespace Tennis {
    public interface TennisGameState {
        TennisGameState WonPoint(string player);

        string GetScore();
    }

    public class TennisGameCP : ITennisGame {
        private readonly string player1;
        private readonly string player2;
        private TennisGameState state;
        
        public TennisGameCP(string player1, string player2) {
            this.player1 = player1;
            this.player2 = player2;
            this.state = new BeforeDeuceState(player1, player2);
        }

        public void WonPoint(string playerName) {
            state = state.WonPoint(playerName);
        }

        public string GetScore() {
            return state.GetScore();
        }
    }
}