namespace Tennis {
    public class DeuceState : TennisGameState {
        public TennisGameState WonPoint(string player) {
            return new AdvantageState(player);
        }

        public string GetScore() {
            return "Deuce";
        }
    }
}