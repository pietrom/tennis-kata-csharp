namespace Tennis {
    public class AdvantageState : TennisGameState {
        private readonly string player;

        public AdvantageState(string player) {
            this.player = player;
        }

        public TennisGameState WonPoint(string player) {
            if (player == this.player) {
                return new WinState(player);
            }
            return new DeuceState();
        }

        public string GetScore() {
            return $"Advantage {player}";
        }
    }
}