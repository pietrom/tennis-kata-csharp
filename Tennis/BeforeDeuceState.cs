using System.Collections.Generic;

namespace Tennis {
    public class BeforeDeuceState : TennisGameState {
        private readonly string player1;
        private readonly string player2;
        private int score1 = 0;
        private int score2 = 0;

        public BeforeDeuceState(string player1, string player2) {
            this.player1 = player1;
            this.player2 = player2;
        }

        public TennisGameState WonPoint(string player) {
            if (player == player1) {
                score1++;
            } else if (player == player2) {
                score2++;
            }

            if (score1 == 3 && score2 == 3) {
                return new DeuceState();
            }

            if (score1 == 4) {
                return new WinState(player1);
            }
            
            if (score2 == 4) {
                return new WinState(player2);
            }

            return this;
        }

        private static readonly IDictionary<int, string> ValuesForScore = new Dictionary<int, string> {
            { 0, "Love" },
            { 1, "Fifteen" },
            { 2, "Thirty" },
            { 3, "Forty" }
        };

        public string GetScore() {
            if (score1 == score2) {
                return $"{ValuesForScore[score1]}-All";
            }

            return $"{ValuesForScore[score1]}-{ValuesForScore[score2]}";
        }
    }
}