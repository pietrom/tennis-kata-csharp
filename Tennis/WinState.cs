namespace Tennis {
    public class WinState : TennisGameState {
        private readonly string player;

        public WinState(string player) {
            this.player = player;
        }

        public TennisGameState WonPoint(string player) {
            throw new System.NotImplementedException();
        }

        public string GetScore() {
            return $"Win for {player}";
        }
    }
}